const path = require('path');
const projectPath = path.resolve(__dirname, '../../../');
const distPackagePath = projectPath + '/public/package/';
const srcPackagePath = projectPath + '/src/webui/package/';
const nodeModulesPath = projectPath + '/tools/webpack/node_modules/';

const UglifyJsPlugin = require(nodeModulesPath + 'uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require(nodeModulesPath + 'mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require(nodeModulesPath + 'optimize-css-assets-webpack-plugin');

module.exports = {
  entry: {
    './common/main': srcPackagePath + 'common/entry.js',
    './default/main': srcPackagePath + 'default/entry.js'
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  output: {
    filename: '[name].js',
    path: distPackagePath
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ],
  module: {
    rules: [
     {
       test: /\.less$/,
       use: [
        { loader: MiniCssExtractPlugin.loader }, 
        { loader: 'css-loader', 
          options: {
            import: false 
          } 
        },
        { loader: 'less-loader' }
       ]
     }
    ]
  }
};
